/*
 *
 * Az albbi probram egy processfarmot hoz letre. A fo program vezerli a
 * dolgozokat es osszegzi a munkat.
 *
 * Parameterek:
 * 1. osszes munka (sec)
 * 2. szakasz (sec)
 * 3. dolgozok szama (>0)
 * 4. elso dolgozo kapacitasa
 * 5. masodik dolgozo ... (opcionalis)
 *
 * */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <wait.h>
#include <signal.h>

int X=23; // osszes munka (sec)
int D=8; // szakasznyi munka (sec)
#define n 3
//int n=3; // dolgozok szama
#define W1 6
#define W2 5
#define W3 4

int WORK_DONE = 0;

pid_t PIDS[n];

void work()
{
	kill(getppid(), SIGUSR1);
}

void ticker()
{
	int i, expected;
	expected=X-WORK_DONE;
	printf("pid=%d received SIGALRM\n", getpid());
	if (expected > 0) {
		for (i=0; i<n; i++) {
			kill(PIDS[i], SIGUSR1);
			expected -= 6; // FIXME - a dolgozohoz kapcsolodo kapacitas (Wx)
			if (expected <= 0) {
				return;
			}
		}
	} else {
		exit(0);
	}
}

void worker_done()
{
	printf("parent: SIGUSR1 received\n");
	WORK_DONE += W1; // FIXME - a dolgozohoz kapcsolodo kapacitas (Wx)
}

pid_t cw() //create worker
{
	pid_t child;

	if ((child = fork()) == 0) {
		while(1) {
			if (signal(SIGUSR1, work) == SIG_ERR) {
				printf("child: SIGUSR1 failed\n");
			}
			pause();
		}
	}
	return child;
}

void help() {
	printf("\n\nElso parameter: osszes munka\n\
Masodik parameter: egysegnyi munka\n\
stb");
}

int main(int argc, char* argv[])
{
	int i=0;

	if (argc < 5) { // NOTE: main always got program name as the first param
		printf("Legalabb 4 parametert meg kell adnia!\n");
		help();
		exit(1);
	}

	if (atoi(argv[3]) != argc-4) {
		printf("A dolgozok szamaval egyezo szamu kapacitast kell megadnia!\n");
		printf("Dolgozok: %s Kapacitasok: %i", argv[3], argc-4);
		help();
		exit(1);
	}

	X = atoi(argv[1]);
	D = atoi(argv[2]);
	//n = atoi(argv[3]);

	while (i<n) {
		PIDS[i] = cw();
		printf("PID: %d\n", PIDS[i]);
		i++;
	}

	if (signal(SIGALRM, ticker) == SIG_ERR) {
		printf("parent: SIGALRM failed\n");
	}
	if (signal(SIGUSR1, worker_done) == SIG_ERR) {
		printf("parent: SIGUSR1 failed\n");
	}
	while(1) {
		while(alarm(D) != 0) {
			printf("KESZ: %i", WORK_DONE);
			pause();
		}
	}
	return 0;
}

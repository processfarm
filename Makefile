NAME = farm

CFLAGS += -g -ggdb -Wall
OBJECTS = main.o

all: $(OBJECTS)
	$(CC) -o $(NAME) $(OBJECTS) $(LIBS)

main.o:
	$(CC) -c main.c $(CFLAGS)

clean:
	rm $(OBJECTS)
	rm $(NAME)

.PHONY: $(OBJECTS) all clean
